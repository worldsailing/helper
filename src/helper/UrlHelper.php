<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Helper;

use Doctrine\Common\Collections\Criteria;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UrlHelper
 * @package worldsailing\Helper
 */
class UrlHelper {

    /**
     * @param array $arr
     * @return bool
     */
    public static function isAssoc(array $arr)
    {
        if (array() === $arr) return false;
        if (! is_array($arr)) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    /**
     * @param Request $request
     * @return int
     */
    public static function getLimitOfRequest(Request $request)
    {
        $limit = ($request->query->get('limit')) ? $request->query->get('limit') : 25 ;

        $page = ($request->query->get('page')) ? $request->query->get('page') : 1 ;

        if ((int) $page === -1) {
            //fetch all
            $limit = 0;
        }

        return (int) $limit;
    }


    /**
     * @param Request $request
     * @return int
     */
    public static function getOffsetOfRequest(Request $request)
    {
        $limit = UrlHelper::getLimitOfRequest($request);

        $page = ($request->query->get('page')) ? $request->query->get('page') : 1 ;

        $offset = 0;

        if ((int) $page !== -1) {
            // NOT fetch all
            if ($page < 1) {
                $page = 1;
            }
            $offset = $limit * ((int) $page - 1 );
        }
        return $offset;
    }


    /**
     * @param Request $request
     * @return array
     */
    public static function getOrderOfRequest(Request $request)
    {
        $order =  ($request->query->get('order')) ? $request->query->get('order') : [] ;

        $result = [];
        if (UrlHelper::isAssoc($order)) {
            if ($order['name'] != '') {
                $result[$order['name']] = ($order['direction'] != '') ? strtoupper($order['direction']) : 'ASC';
            }
        } else {
            foreach ($order as $item) {
                if ($item['name'] != '') {
                    $result[$item['name']] = ($item['direction'] != '') ? strtoupper($item['direction']) : 'ASC';
                }
            }
        }
        return $result;
    }


    /**
     * @param Request $request
     * @return Criteria
     */
    public static function getCriteriaOfRequest(Request $request)
    {
        /**
         * TODO: Not implemented
         */
        return new Criteria();
    }
}
