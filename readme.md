# worldsailing Helper 

### PHP library description
Helper bundle to World Sailing microservices based on Silex.

  
### Install by composer
```php
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://worldsailing@bitbucket.org/worldsailing/helper.git"
    }
  ],
  "require": {
    "worldsailing/helper": "dev-master"
  }
}
```
 
### License

All right reserved
